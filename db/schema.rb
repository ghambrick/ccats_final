# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150802233936) do

  create_table "appointments", force: :cascade do |t|
    t.integer  "job_id"
    t.datetime "date"
    t.text     "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chasses", force: :cascade do |t|
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "colors", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "phone"
    t.string   "email"
    t.string   "street"
    t.string   "city"
    t.string   "state",      default: "TX"
    t.string   "zip"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "emp_jobs", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "job_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "phone"
    t.string   "email"
    t.string   "street"
    t.string   "city"
    t.string   "state",                              default: "TX"
    t.string   "zip"
    t.date     "start_date"
    t.boolean  "status"
    t.decimal  "pay_rate",   precision: 6, scale: 2
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  create_table "engines", force: :cascade do |t|
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "customer_id"
    t.date     "date"
    t.decimal  "subtotal",    precision: 6, scale: 2, default: 0.0
    t.decimal  "adjustment",  precision: 6, scale: 2, default: 0.0
    t.decimal  "tax",         precision: 6, scale: 2
    t.decimal  "total",       precision: 6, scale: 2
    t.boolean  "paid"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  create_table "jobs", force: :cascade do |t|
    t.integer  "vehicle_id"
    t.integer  "employee_id"
    t.integer  "invoice_id"
    t.decimal  "amount",      precision: 6, scale: 2, default: 0.0
    t.boolean  "complete"
    t.text     "comments"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  create_table "line_items", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "employee_id"
    t.integer  "service_id"
    t.decimal  "labor_hrs",   precision: 4, scale: 2
    t.decimal  "charge",      precision: 6, scale: 2
    t.boolean  "complete"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "makes", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "models", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reports", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "desc"
    t.decimal  "price",      precision: 6, scale: 2
    t.decimal  "flag_hrs",   precision: 4, scale: 2
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "surplus_products", force: :cascade do |t|
    t.integer  "job_id"
    t.text     "part_description"
    t.string   "image_url"
    t.decimal  "price",              precision: 7, scale: 2
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "transmissions", force: :cascade do |t|
    t.string   "style"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vehicles", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "make_id"
    t.integer  "model_id"
    t.integer  "year_id"
    t.integer  "color_id"
    t.integer  "engine_id"
    t.integer  "transmission_id"
    t.integer  "chassis_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "years", force: :cascade do |t|
    t.integer  "mod_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
