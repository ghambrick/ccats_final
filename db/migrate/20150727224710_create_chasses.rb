class CreateChasses < ActiveRecord::Migration
  def change
    create_table :chasses do |t|
      t.string :code, :length => 100

      t.timestamps null: false
    end
  end
end
