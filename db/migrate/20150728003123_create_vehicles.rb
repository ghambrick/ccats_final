class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :customer_id
      t.integer :make_id
      t.integer :model_id
      t.integer :year_id
      t.integer :color_id
      t.integer :engine_id
      t.integer :transmission_id
      t.integer :chassis_id

      t.timestamps null: false
    end
  end
end
