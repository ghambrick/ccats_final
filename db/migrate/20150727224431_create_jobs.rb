class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :vehicle_id
      t.integer :employee_id
      t.integer :invoice_id
      t.decimal :amount, default: 0, :precision => 6, :scale => 2
      t.boolean :complete
      t.text :comments

      t.timestamps null: false
    end
  end
end
