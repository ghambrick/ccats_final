class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :job_id
      t.integer :employee_id
      t.integer :service_id
      t.decimal :labor_hrs, :precision => 4, :scale => 2
      t.decimal :charge, :precision => 6, :scale => 2
      t.boolean :complete

      t.timestamps null: false
    end
  end
end
