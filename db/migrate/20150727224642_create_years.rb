class CreateYears < ActiveRecord::Migration
  def change
    create_table :years do |t|
      t.integer :mod_year

      t.timestamps null: false
    end
  end
end
