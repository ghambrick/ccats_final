class CreateEmpJobs < ActiveRecord::Migration
  def change
    create_table :emp_jobs do |t|
      t.integer :employee_id
      t.integer :job_id

      t.timestamps null: false
    end
  end
end
