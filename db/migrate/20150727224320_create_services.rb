class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :desc, :length => 100
      t.decimal :price, :precision => 6, :scale => 2
      t.decimal :flag_hrs, :precision => 4, :scale => 2

      t.timestamps null: false
    end
  end
end
