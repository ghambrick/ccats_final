require 'test_helper'

class SurplusProductsControllerTest < ActionController::TestCase
  setup do
    @surplus_product = surplus_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:surplus_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create surplus_product" do
    assert_difference('SurplusProduct.count') do
      post :create, surplus_product: { image_url: @surplus_product.image_url, job_id: @surplus_product.job_id, part_description: @surplus_product.part_description, price: @surplus_product.price }
    end

    assert_redirected_to surplus_product_path(assigns(:surplus_product))
  end

  test "should show surplus_product" do
    get :show, id: @surplus_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @surplus_product
    assert_response :success
  end

  test "should update surplus_product" do
    patch :update, id: @surplus_product, surplus_product: { image_url: @surplus_product.image_url, job_id: @surplus_product.job_id, part_description: @surplus_product.part_description, price: @surplus_product.price }
    assert_redirected_to surplus_product_path(assigns(:surplus_product))
  end

  test "should destroy surplus_product" do
    assert_difference('SurplusProduct.count', -1) do
      delete :destroy, id: @surplus_product
    end

    assert_redirected_to surplus_products_path
  end
end
