class SurplusProductsController < ApplicationController
  before_action :set_surplus_product, only: [:show, :edit, :update, :destroy]

  # GET /surplus_products
  # GET /surplus_products.json
  def index
    @surplus_products = SurplusProduct.all
  end

  # GET /surplus_products/1
  # GET /surplus_products/1.json
  def show
  end

  # GET /surplus_products/new
  def new
    @surplus_product = SurplusProduct.new
  end

  # GET /surplus_products/1/edit
  def edit
  end

  # POST /surplus_products
  # POST /surplus_products.json
  def create
    @surplus_product = SurplusProduct.new(surplus_product_params)

    respond_to do |format|
      if @surplus_product.save
        format.html { redirect_to @surplus_product, notice: 'Surplus product was successfully created.' }
        format.json { render :show, status: :created, location: @surplus_product }
      else
        format.html { render :new }
        format.json { render json: @surplus_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /surplus_products/1
  # PATCH/PUT /surplus_products/1.json
  def update
    respond_to do |format|
      if @surplus_product.update(surplus_product_params)
        format.html { redirect_to @surplus_product, notice: 'Surplus product was successfully updated.' }
        format.json { render :show, status: :ok, location: @surplus_product }
      else
        format.html { render :edit }
        format.json { render json: @surplus_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /surplus_products/1
  # DELETE /surplus_products/1.json
  def destroy
    @surplus_product.destroy
    respond_to do |format|
      format.html { redirect_to surplus_products_url, notice: 'Surplus product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_surplus_product
    @surplus_product = SurplusProduct.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def surplus_product_params
    params.require(:surplus_product).permit(:job_id, :part_description, :image_url,  :photo, :price)
  end
end