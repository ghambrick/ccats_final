class EmpJobsController < ApplicationController
  before_action :set_emp_job, only: [:show, :edit, :update, :destroy]

  # GET /emp_jobs
  # GET /emp_jobs.json
  def index
    @emp_jobs = EmpJob.all
  end

  # GET /emp_jobs/1
  # GET /emp_jobs/1.json
  def show
  end

  # GET /emp_jobs/new
  def new
    @emp_job = EmpJob.new
  end

  # GET /emp_jobs/1/edit
  def edit
  end

  # POST /emp_jobs
  # POST /emp_jobs.json
  def create
    @emp_job = EmpJob.new(emp_job_params)

    respond_to do |format|
      if @emp_job.save
        format.html { redirect_to @emp_job, notice: 'Emp job was successfully created.' }
        format.json { render :show, status: :created, location: @emp_job }
      else
        format.html { render :new }
        format.json { render json: @emp_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emp_jobs/1
  # PATCH/PUT /emp_jobs/1.json
  def update
    respond_to do |format|
      if @emp_job.update(emp_job_params)
        format.html { redirect_to @emp_job, notice: 'Emp job was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_job }
      else
        format.html { render :edit }
        format.json { render json: @emp_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_jobs/1
  # DELETE /emp_jobs/1.json
  def destroy
    @emp_job.destroy
    respond_to do |format|
      format.html { redirect_to emp_jobs_url, notice: 'Emp job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_job
      @emp_job = EmpJob.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_job_params
      params.require(:emp_job).permit(:employee_id, :job_id)
    end
end
