json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :customer_id, :date, :subtotal, :adjustment, :tax, :total, :paid
  json.url invoice_url(invoice, format: :json)
end
