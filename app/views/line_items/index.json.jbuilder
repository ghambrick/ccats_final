json.array!(@line_items) do |line_item|
  json.extract! line_item, :id, :job_id, :employee_id, :service_id, :labor_hrs, :charge, :complete
  json.url line_item_url(line_item, format: :json)
end
