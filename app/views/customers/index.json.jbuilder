json.array!(@customers) do |customer|
  json.extract! customer, :id, :fname, :lname, :phone, :email, :street, :city, :state, :zip
  json.url customer_url(customer, format: :json)
end
