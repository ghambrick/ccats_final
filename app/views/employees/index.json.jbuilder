json.array!(@employees) do |employee|
  json.extract! employee, :id, :fname, :lname, :phone, :email, :street, :city, :state, :zip, :start_date, :status, :pay_rate
  json.url employee_url(employee, format: :json)
end
