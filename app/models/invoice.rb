class Invoice < ActiveRecord::Base

  has_many :jobs

  belongs_to :customer


  def show_paid
    if paid?
      "Paid"
    else
      "Due"
    end
  end

end
