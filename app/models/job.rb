class Job < ActiveRecord::Base

  has_many :appointments
  has_many :line_items, dependent: :destroy
  has_many :surplus_products
  # has_many :emp_jobs
  # has_many :employees, through: :emp_jobs

  belongs_to :employee
  belongs_to :vehicle
  belongs_to :invoice, dependent: :destroy


  before_create :auto_create_invoice
  after_update :update_invoice_subtotal


  #returns custom string instead of default boolean status
  def show_complete
    if complete?
      "Finished"
    else
      "In Progress"
    end
  end

  #when a job is marked complete, update invoice subtotal
  def update_invoice_subtotal
    if self.complete?
      Invoice.update(self.invoice_id, :subtotal => self.amount)

      newTax = self.amount * 0.0625

      Invoice.update(self.invoice_id, :tax => newTax)

      newTotal = self.amount + newTax

      Invoice.update(self.invoice_id, :total => newTotal)
    end
  end

  def auto_create_invoice
    if self.invoice_id.nil?
      self.invoice = Invoice.create(customer_id: self.vehicle.customer_id)
    end
  end

  def list_open_jobs
    select([:job_id, :job_comments]).where(complete: false)


  end



end
