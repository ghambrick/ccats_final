class SurplusProduct < ActiveRecord::Base

  belongs_to :job

  belongs_to :customer

  has_attached_file :photo,

                    :path => "public/images/:class/:id/:filename",
                    :url => "/images/:class/:id/:basename.:extension"


end