class Employee < ActiveRecord::Base

  # has_many :emp_jobs
  # has_many :jobs, through: :emp_jobs

  has_many :jobs
  has_many :line_items


  def show_full_name
    "#{fname} #{lname}"
  end


  def show_active
    if status?
      "Active"
    else
      "Inactive"
    end
  end
end
