class Report < ActiveRecord::Base

  def invoice_totals
    self.invoice.select("sum(total).group(self.invoice_id)")
  end

  def show_veh_info
    "#{self.customer.show_full_name}  - #{self.year.mod_year} #{self.make.name} #{self.model.name} #{self.color.name}"
  end

end
