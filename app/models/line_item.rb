class LineItem < ActiveRecord::Base

  belongs_to :job
  belongs_to :service
  belongs_to :employee

  after_save :update_job_amount
  after_save :update_job_status
  after_update :update_job_amount_subtract





  #returns custom string instead of default boolean status
  def show_complete
    if complete?
      "Complete"
    else
      "In Progress"
    end
  end



  #update the Job amount when it's corresponding line item is marked complete
  def update_job_amount

    if self.complete?

      #capture the price of this line item
      x = self.charge
      #find and store corresponding Job
      j = Job.find(self.job_id)

      new_amount = j.amount + x

      #update its amount with the addition of this line items amount
      Job.update(self.job_id, :amount => new_amount)


    end
  end



  #update the Job amount when it's corresponding line item is marked complete
  def update_job_amount_subtract

    if self.complete == false

      #capture the price of this line item
      x = self.charge
      #find and store corresponding Job
      j = Job.find(self.job_id)

      new_amount = j.amount - x

      #update its amount with the addition of this line items amount
      Job.update(self.job_id, :amount => new_amount)

    end
  end



  def update_job_status
    x = true
    j = Job.find(self.job_id)

    j.line_items.each do |line|
      if line.complete?
      else
        x = false
      end

      j.complete = x
      j.save
    end
  end



  def self.compare_hours

    hours = 0
    LineItem.all.each do |line|
      line.complete?
      hours += line.labor_hrs - Service.find(line.service_id).flag_hrs
    end

    return hours
  end



end