class Appointment < ActiveRecord::Base

  belongs_to :job


  validates_uniqueness_of :date, scope: [], :message=>"This time is already taken. Please select another time."

end
