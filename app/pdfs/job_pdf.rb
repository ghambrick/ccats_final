class JobPdf < Prawn::Document
  require 'prawn/table'
  def initialize(job, view)
    super()
    @job = job
    @view = view
    logo
    job_number
    thanks_message
    line_row_header
    line_row_table

  end

   def logo
     logopath =  "#{Rails.root}/app/assets/images/sponsor.logo.jpg"
     image logopath, :width => 197, :height => 91
     move_down 10
     draw_text "Job details", :at => [220, 575], size: 22
   end

  def  job_number
    text "Job Number:
     #{@job.id} ", :size => 13
    move_down 20
  end

   def thanks_message
     move_down 40
     text "Thank you, this is a listing of services provided.",
          :indent_paragraphs => 40, :size => 13
   end

  def line_row_header
    move_down 80
   text "Service            Technician  Labor Hours  Charge Complete"
  end

  def line_row_table
        @job.line_items.each do | j |
         [j.service.desc, j.employee.show_full_name, j.labor_hrs, j.charge, j.show_complete]
        end
  end

  def line_row_table_data
    [line_row_header, *line_row_table]

  end

  def table_content
      table line_rows do
      row(0).font_style = :bold
      self.header = true
      self.row_colors = ['DDDDDD', 'FFFFFF']
      self.column_widths = [400, 150, 20, 20]
    end
  end

  def line_rows
    [['Service', 'Technician', 'Labor Hours', 'Charge']] +
        @job.line_items.map do |product|
          [product.id, product.name, product.price]
        end
  end


end
